import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Routes, Route } from "react-router-dom";
import axios from "axios";
import Header from "./components/header/Header";
import ProductList from "./components/ProductList/ProductList";
import Cart from "./components/Cart/Cart";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";
import modalTemplates from "./components/modal/modalTemplates.js";
import Favorites from "./components/favorites/Favorites";
import {
  //RECEIVED_LIST_GOODS,
  OPENED_MODAL_WINDOW,
  CLOSED_MODAL_WINDOW,
  RECEIVED_ID_GOODS_IN_CART,
  RECEIVED_ID_GOODS_IN_FAVORITES,
  //SELECTED_ID_ITEM_FOR_ACTIONS,
  RECEIVED_PROPERTIES_MODAL_WINDOWS,
  //RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW,
} from "./redux/market.actions";

const Market = () => {
  const state = useSelector((state) => state);
  const cartProdsId = useSelector((state) => state.cartProdsId);
  const targetProd = useSelector((state) => state.targetProd);
  const classWrapper = useSelector((state) => state.classWrapper);
  const modalDeclaration = useSelector((state) => state.modalDeclaration);

  //const cards = useSelector((state) => state.cards);
  const dispatch = useDispatch();

  const [cards, setCards] = useState([]);

  useEffect(() => {
    axios("/airplane.json").then((res) => {
      setCards(res.data);
      // dispatch({
      //     type: RECEIVED_LIST_GOODS,
      //     payload: res.data,
      // })
    });
    if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", "[]");
  }, []);

  useEffect(() => {
    const [modalWindow] = Object.values(modalTemplates);
    dispatch({
      type: RECEIVED_PROPERTIES_MODAL_WINDOWS,
      payload: modalWindow,
    });
  }, []);

  useEffect(() => {
    const cartProdsIdUpdate = JSON.parse(localStorage.getItem("cart"));
    dispatch({
      type: RECEIVED_ID_GOODS_IN_CART,
      payload: cartProdsIdUpdate,
    });
  }, []);

  useEffect(() => {
    const favoritesProd = JSON.parse(localStorage.getItem("favorites"));
    dispatch({
      type: RECEIVED_ID_GOODS_IN_FAVORITES,
      payload: favoritesProd,
    });
  }, []);

  // background of closing content when modal is running
  const didMount = () => {
    dispatch({
      type: OPENED_MODAL_WINDOW,
    });
  };

  const didUnmounted = () => {
    dispatch({
      type: CLOSED_MODAL_WINDOW,
    });
  };

  const closeModal = () => {
    dispatch({
      type: CLOSED_MODAL_WINDOW,
    });
  };
  // ----------------------------------

  // activity of a button of a modal window depending on its type
  const onClickModalBtn = (e) => {
    let newProds = cartProdsId.map((element) => element);
    if (e.target.dataset.modalId === "modalID2") {
      newProds.push(targetProd);
      localStorage.setItem("cart", JSON.stringify(newProds));
    }
    if (e.target.dataset.modalId === "modalID3") {
      newProds = cartProdsId.map((element) => element);
      if (newProds.includes(targetProd)) {
        newProds = newProds.filter((item) => item !== targetProd);
        localStorage.setItem("cart", JSON.stringify(newProds));
      }
    }
    dispatch({
      type: CLOSED_MODAL_WINDOW,
    });
    dispatch({
      type: RECEIVED_ID_GOODS_IN_CART,
      payload: newProds,
    });
  };
  //-----------------------------------------

  //array typing for components cart and favorites
  const filterBy = (a, b) => {
    let typedArr = a.filter(function (a) {
      return a.id === b.find((item) => item === a.id);
    });
    return typedArr;
  };
  //--------------------------------------------------

  const {
    className,
    id,
    header,
    closeButton,
    description,
    classNameButton,
    textButtonLeft,
    textButtonRight,
  } = modalDeclaration;

  return (
    <>
      <div className={classWrapper} onClick={closeModal}></div>
      <Header />
      <div className="content">
        <Routes>
          <Route
            path="/"
            element={<ProductList cards={cards} closeButtonAddCart={true} />}
          ></Route>
          <Route
            path="cart"
            element={<Cart cards={cards} filter={filterBy} />}
          ></Route>
          <Route
            path="favorites"
            element={<Favorites cards={cards} filter={filterBy} />}
          ></Route>
        </Routes>
      </div>

      {state.displayState === "open" && (
        <Modal
          className={className}
          id={id}
          onClick={closeModal}
          show={didMount}
          hidden={didUnmounted}
          header={header}
          closeButton={closeButton}
          description={description}
          actions={
            <>
              <Button
                id={id}
                className={classNameButton}
                dataModal={id}
                text={textButtonLeft}
                onClick={onClickModalBtn}
              />
              <Button
                className={classNameButton}
                text={textButtonRight}
                onClick={closeModal}
              />
            </>
          }
        />
      )}
    </>
  );
};

export default Market;

import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import Market from "./Market"
import marketReducer from "./redux/market.reducer"

const store = createStore(marketReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const App = () => {

  return (
    
    <BrowserRouter>
      <Provider store={store}>
        <Market />
      </Provider>
    </BrowserRouter>
    
  
  );
};

export default App;


import {
  RECEIVED_LIST_GOODS,
  OPENED_MODAL_WINDOW,
  CLOSED_MODAL_WINDOW,
  RECEIVED_ID_GOODS_IN_CART,
  RECEIVED_ID_GOODS_IN_FAVORITES,
  SELECTED_ID_ITEM_FOR_ACTIONS,
  RECEIVED_PROPERTIES_MODAL_WINDOWS,
  RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW,
  RECEIVED_PRODUCT_LIST,
} from "./market.actions.js";

const initialState = {
  cards: null,
  classWrapper: "",
  displayState: "none",
  cartProdsId: null,
  favoritesListId: null,
  targetProd: null,
  modalWindowDeclarations: null,
  modalDeclaration: {},
  productsList: {},
  modalState: "none",
};

const marketReducer = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVED_LIST_GOODS:
        return {
            ...state,
            cards: action.payload,
        }

    case RECEIVED_ID_GOODS_IN_CART:
      return {
        ...state,
        cartProdsId: action.payload,
      };

    case RECEIVED_ID_GOODS_IN_FAVORITES:
      return {
        ...state,
        favoritesListId: action.payload,
      };

    case SELECTED_ID_ITEM_FOR_ACTIONS:
      return {
        ...state,
        targetProd: action.payload,
      };

    case OPENED_MODAL_WINDOW:
      return {
        ...state,
        classWrapper: "wrapper",
        displayState: "open",
        
      };

    case CLOSED_MODAL_WINDOW:
      return {
        ...state,
        classWrapper: "",
        displayState: "none",
      };

      case RECEIVED_PROPERTIES_MODAL_WINDOWS:
      return {
          ...state,
          modalWindowDeclarations: action.payload,
      };

      case RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW:
      return {
          ...state,
          modalDeclaration: action.payload,
      };

      case RECEIVED_PRODUCT_LIST:
      return {
          ...state,
          productsList: action.payload,
      };
      
      default:
      return state;
  }
};

export default marketReducer;

import React from "react";
import { useSelector } from "react-redux";
import ProductList from "../ProductList/ProductList";

function Favorites(props) {
  const {
    cards,
    filter,
  } = props;
  const favoritesListId = useSelector((state) => state.favoritesListId);
  const favoritesListProd = filter(cards, favoritesListId);

  return (
    <>
      <h2>YOUR FAVORITES</h2>
      <ProductList
        cards={favoritesListProd}
        closeButtonAddCart={true}
      />
    </>
  );
}

export default Favorites;

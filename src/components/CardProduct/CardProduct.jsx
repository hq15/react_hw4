import React from "react";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import "./CardProduct.scss";
import {
  RECEIVED_ID_GOODS_IN_FAVORITES,
  RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW,
  SELECTED_ID_ITEM_FOR_ACTIONS,
  OPENED_MODAL_WINDOW,
} from "../../redux/market.actions";
import Button from "../button/Button.jsx";
import "../button/Button.scss";
import WishlistIcon from "../wishlistIcon/WishlistIcon.jsx";

const CardProduct = (props) => {
  const favoritesListId = useSelector((state) => state.favoritesListId);
  const modalWindowDeclarations = useSelector(
    (state) => state.modalWindowDeclarations
  );
  const dispatch = useDispatch();

  const {
    card,
    closeButtonAddCart,
    className,
    classNameIcon,
    isItemInCart,
  } = props;

  const openModal = (e) => {
    const modalID = e.target.dataset.modalId;
    let modalTarget = modalWindowDeclarations.find(
      (item) => item.id === modalID
    );

    dispatch({
      type: RECEIVED_PROPERTIES_ACTIVE_MODAL_WINDOW,
      payload: modalTarget,
    });
    dispatch({
      type: SELECTED_ID_ITEM_FOR_ACTIONS,
      payload: e.target.id,
    });
    dispatch({
      type: OPENED_MODAL_WINDOW,
    });
  };

  const addToFavorites = (e) => {
    const targetId = e.target.parentElement.id;
    let newProducts = favoritesListId.map((targetId) => targetId);
    if (newProducts.includes(targetId)) {
      newProducts = newProducts.filter((item) => item !== targetId);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    } else {
      newProducts.push(targetId);
      localStorage.setItem("favorites", JSON.stringify(newProducts));
    }
    dispatch({
      type: RECEIVED_ID_GOODS_IN_FAVORITES,
      payload: newProducts,
    });
  };

  return (
    <div key={card.id} className={className} id={card.id}>
      <h3 className="product-name">{card.ProductName}</h3>
      <div className="product-card-container">
        <div>
          <div className="img-wrapper">
            <img
              className="product-img"
              src={card.link}
              alt={card.ProductName}
            />
          </div>
        </div>
        <div>
          <p className="product-article">Model: {card.type}</p>
          <p>Price: {card.price} USD</p>
          <p>Color: {card.color}</p>
        </div>
      </div>
      <div id={card.id} className="product-activity">
        {closeButtonAddCart && (
          <Button
            id={card.id}
            className={` btn ${
              isItemInCart ? "btn__added-to-card" : "btn__add-to-card"
            }`}
            dataModal={"modalID2"}
            text={`${isItemInCart ? "Added to cart" : "Add to cart"}`}
            onClick={isItemInCart ? null : openModal}
          />
        )}

        {isItemInCart && (
          <button
            id={card.id}
            className="btn__close"
            data-modal-id={"modalID3"}
            onClick={openModal}
          >
            X
          </button>
        )}
        <WishlistIcon
          id={card.id}
          classNameIcon={classNameIcon}
          isFavorites={card.isFavorites}
          onClick={addToFavorites}
        />
      </div>
    </div>
  );
};

CardProduct.propTypes = {
  card: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  classNameIcon: PropTypes.string,
  favorites: PropTypes.func,
  isFavorites: PropTypes.bool,
  className: PropTypes.string,
};
CardProduct.defaultProps = {
  classNameIcon: "favor-icon",
  className: "product-card",
  isFavorites: false,
};

export default CardProduct;

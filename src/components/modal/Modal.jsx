import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { OPENED_MODAL_WINDOW, CLOSED_MODAL_WINDOW } from "../../redux/market.actions";
import PropTypes from "prop-types";
import "./Modal.scss";

function Modal(props) {
  
  const dispatch = useDispatch();


  const {
    className,
    header,
    closeButton,
    onClick,
    description,
    actions,
    show,
    hidden,
  } = props;


  useEffect(() => {
    show();
    dispatch({
      type: OPENED_MODAL_WINDOW,
    });
    return function cleanup() {
      hidden();
      dispatch({
        type: CLOSED_MODAL_WINDOW,
      });
    };
  }, []);

  return (
    <>
      <div className={className}>
        <div className="modal__header">
          <h3>{header}</h3>
          {closeButton && (
            <button className="btn__close" onClick={onClick}>
              X
            </button>
          )}
        </div>
        <p className="modal__text">{description}</p>
        {actions}
      </div>
    </>
  );
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  actions: PropTypes.object.isRequired,
  closeButton: PropTypes.bool,
};

Modal.defaultProps = {
  closeButton: false,
};

export default Modal;

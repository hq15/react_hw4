import React from "react";
import { useSelector } from "react-redux";
import ProductList from "../ProductList/ProductList";

const Cart = (props) => {
  const {
    cards,
    filter,
  } = props;

  const cartProdsId = useSelector((state) => state.cartProdsId);
  //const favoritesListId = useSelector((state) => state.favoritesListId);
  const cartProducts = filter(cards, cartProdsId);

  return (
    <>
      <h2>YOUR CART</h2>
      <ProductList
        cards={cartProducts}
        closeButtonAddCart={false}
      />
    </>
  );
};

export default Cart;

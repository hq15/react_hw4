import React from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import CardProduct from "../CardProduct/CardProduct";
//import { RECEIVED_ID_GOODS_IN_FAVORITES } from "../../redux/market.actions";
import "./ProductList.scss";

function ProductList(props) {
  const favoritesListId = useSelector((state) => state.favoritesListId);
  const cartProds = useSelector((state) => state.cartProdsId);

  const { cards, closeButtonAddCart } = props;

  const cardsWithIsFavorites = cards.map((element) => {
    favoritesListId.includes(element.id)
      ? (element.isFavorites = true)
      : (element.isFavorites = false);
    return element;
  });
  const productsList = cardsWithIsFavorites;

  const cardsList = productsList.map((e) => (
    <CardProduct
      key={e.id}
      card={e}
      closeButtonAddCart={closeButtonAddCart}
      isItemInCart={cartProds.includes(e.id)}
    />
  ));
  return <div className="products-wrapper">{cardsList}</div>;
}

ProductList.propTypes = {
  cards: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    })
  ).isRequired,
  onClick: PropTypes.func,
  closeButtonAddCart: PropTypes.bool,
};

ProductList.defaultProps = {
  onClick: null,
  closeButtonAddCart: true,
};

export default ProductList;

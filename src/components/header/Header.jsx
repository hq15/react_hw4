import React from "react";
import "./Header.scss";
import logo from "../../img/logo.png";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <img className="header__logo" src={logo} alt="planesales" />
      <h1 className="header__title">The Global Aircraft Marketplace</h1>
      <nav>
        <ul className="header__menu">
          <Link to="/">Products</Link>
          <Link to="cart">Cart</Link>
          <Link to="favorites">Favorites</Link>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
